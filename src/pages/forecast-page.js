import { database } from '../helpers/database.js';
import { config } from '../helpers/config.js';

const displayFiveDaysForecast = (data) => {
  const listThreeHoursForecast = data.list;
  const forecast = [];
  let timePosition = 0;

  if (listThreeHoursForecast.length < 40) {
    timePosition = listThreeHoursForecast.length-4;
  } else {
    timePosition = listThreeHoursForecast.length-1;
  }

  for (let i = 0; i < 5; i++) {
    const size = 8 % listThreeHoursForecast.length;
    timePosition = (timePosition + size) % listThreeHoursForecast.length;

    forecast.push(listThreeHoursForecast[timePosition]);
    console.log(timePosition);
  }

  console.log(forecast);

  for (const day of forecast) {
    const dateArray = day.dt_txt.split(' ');
    const [date, hours] = dateArray;
    const convertedDate = new Date(date).toDateString();
    const dateAsDay = `${convertedDate.slice(0, 3)} ${convertedDate.slice(8, 10)}`;
    $(config.selectors.fiveDays).append(
        `<div class="forecast-display">
          <p class="forecast-day">${dateAsDay}</p>
          <p class="forecast-temperature">
          <img class="forecast-icon" src="${config.urls.iconUrl}${day.weather[0].icon}" alt="Forecast weather icon">
              ${Math.round(day.main.temp)}°
          </p>
          <p class="forecast-description">${day.weather[0].description}</p>
          <p class="update-info-hours">time of calculation: ${hours.slice(0, 5)}</p>
        </div>`
    );
    // console.log(day);
  }
};

 console.log(database.getWeatherByForecast('Sofia'));
const render = (cityName) => {
  database.getWeatherByForecast(cityName)
      .then(displayFiveDaysForecast)
      .fail(()=>{
        // eslint-disable-next-line no-alert
        alert('The forecast data is not available. We apologise for the inconvenience!');
      });
};

export const forecastPage = {
  render,
};
