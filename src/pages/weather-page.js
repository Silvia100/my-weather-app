import { database } from '../helpers/database.js';
import { config } from '../helpers/config.js';

/*const htmlWeatherPage = {
  container: $('<div></div>'),
  head: $('<h1></h1>'),
  temperature: {
    currentTemp: $('<p></p>'),
    maxTemp: $('<span class="max-temp"></span>'),
    minMaxTemp: $('<p class="min-max-temp"></p>'),
  },
  additionalInfo: {
    country: $('<span></span>'),
    currentDescription: $('<p></p>'),
    currentHumidity: $('<p class="current-humidity-pressure"></p>'),
    pressure: $('<span class="current-pressure"></span>'),
  },
  icon: $('<img>'),
};

$(config.selectors.currentDay)
    .append(htmlWeatherPage.container).attr('id', 'current-day-weather ')
    .append(htmlWeatherPage.head).addClass('current-city')
    .append({
      (htmlWeatherPage.temperature.currentTemp).addClass('current-temperature').text(`${data.name},`),
      (htmlWeatherPage.additionalInfo.country).addClass('country').text(`${data.sys.country}`)
    })
    .append({
      (htmlWeatherPage.icon).addClass('current-icon').attr({src: `${config.urls.iconUrl}${data.weather[0].icon}`, alt: 'current weather icon' }),
      .text(`${Math.round(data.main.temp)}°`)
    })
    .append(htmlWeatherPage.currentDescription).addClass()*/




const displayCurrentDayData = (data) => {
  $(config.selectors.currentDay)
      .html(`<div id="current-day-weather">
              <h1 class="current-city">${data.name}, <span class="country">${data.sys.country}</span></h1>
                <p class="current-temperature">
                  <img class="current-icon" src="${config.urls.iconUrl}${data.weather[0].icon}" alt="current weather icon">
                  ${Math.round(data.main.temp)}°
                 <!-- <button class="toggle-degree-selector">C</button>-->
                </p>
                <p class="current-description">${data.weather[0].description}</p>
                <p class="min-max-temp">min ${Math.round(data.main.temp_min)}° | <span class="max-temp">max ${Math.round(data.main.temp_max)}°</span></p>
                <p class="current-humidity-pressure">Humidity: ${data.main.humidity}% <span class="current-pressure">Pressure: ${data.main.pressure} mb</span></p>
            </div>`);
};

const render = (cityName) => {
  database.getWeatherByName(cityName)
      .then(displayCurrentDayData);
// .fail(alert('The forecast data is not available. We apologise for the inconvenience!'));
};

export const weatherPage = {
  render,
};


