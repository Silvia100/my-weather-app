import { weatherPage } from './pages/weather-page.js';
import { forecastPage } from './pages/forecast-page.js';

let currentCity = 'Sofia';
let favoriteCity = null;

/* -- favorite city -- */

const addFavorite = (city) => {
  $('.fa-star').on('click', (e) => {
    $(e.target).addClass('active');
    localStorage.setItem('favoriteCity', city );
    // console.log(favoriteCity);
  });
};

const renderFavoriteCity = () => {
  favoriteCity = localStorage.getItem('favoriteCity');
  if (favoriteCity) {
    weatherPage.render(favoriteCity);
    forecastPage.render(favoriteCity);
    $('.fa-star').addClass('active');
  } else {
    weatherPage.render(currentCity);
    forecastPage.render(currentCity);
  }
};
renderFavoriteCity();

/* -- search location --*/

$('#search-location').on('click', () => {
  currentCity = $('#location-name').val();
  renderLocation(currentCity);
  addFavorite(currentCity);
});


/* -- predefined locations -- */

const listOfElements = [...$('li')];

listOfElements.forEach((element) => {
  $(element).on('click', (e) => {
    e.preventDefault();
    currentCity = $(element).text();
    renderLocation(currentCity);
    addFavorite(currentCity);
  });
});

const renderLocation = (location) => {
  $('.forecast-display').remove();
  $('.fa-star').removeClass('active');
  weatherPage.render(location);
  forecastPage.render(location);
};
