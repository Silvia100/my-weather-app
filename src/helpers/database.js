import { config } from './config.js';


const getWeatherByName = (cityName) => {
  return $.get(`${config.urls.weatherUrl}weather?q=${cityName}&appid=${config.keyApi}&units=${config.temperature.tempCelsius}`);
};

const getWeatherByForecast = (cityName) => {
  return $.get(`${config.urls.weatherUrl}forecast?q=${cityName}&appid=${config.keyApi}&units=${config.temperature.tempCelsius}`);
};


export const database = {
  getWeatherByName,
  getWeatherByForecast,
};


/* const getLocation = () => {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    console.log('geolocation is not supported');
  }
};

const showPosition = (position) => {
  let lat = position.coords.latitude;
  let lon = position.coords.longitude;
  console.log((`${config.url}weather?lat=${lat}&lon=${lon}&appid=${config.keyApi}`));
  return $.get(`${config.url}weather?lat=${lat}&lon=${lon}&appid=${config.keyApi}`);
}; */
// console.log($.get(`${config.url}weather?q=Sofia&appid=${config.keyApi}&units=${config.tempCelsius}`));
