export const config = {
  urls: {
    weatherUrl: 'http://api.openweathermap.org/data/2.5/',
    iconUrl: 'http://api.openweathermap.org/img/w/',
  },
  keyApi: '30b7469b657aaf798a417576bd61db29',
  temperature: {
    tempCelsius: 'metric',
    tempFahrenheit: 'imperial',
  },
  selectors: {
    currentDay: '#current-day',
    fiveDays: '#five-days-forecast',
  },
};

