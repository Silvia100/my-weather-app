# Weather App #

**The Weather App** is the first project from Telerik Academy in AlphaJavaScriptFeb2019 cohort.  

For this project we used [OpenWeatherMap API](https://openweathermap.org/api).  

## Project Overview ##

_The goal_ of the application is to present weather forecast for current day, five days forecast and hourly forecast.  
Users could check the weather in different locations around the globe.  
On the navigation bar there are two sections with predefined locations: BG - with four Bulgarian major cities, and EU - with four European capital cities. There is also a search menu, where users can make custom searches for locations provided by OpenWeatherMap API, also a button for adding city as a favorite location.
Sofia city will be displayed as a current location when the user opens the application for first time or does not select a favorite city. If a city is selected as a favorite the weather forecast will be available and displayed after loading for the next visits.  

#### How to start ####

* download or clone the project
* install all dependencies with `npm install`
* start the development server with `npm start`  

#### Dependencies ####

* Bootstrap
* jQuery
* Google Fonts

#### Additional info ####

This project has three branches. Every branch represents a weather application created personally by every member of the Team 12.  
The idea was everyone to put in practice the knowledge learned from Module 1 of the Academy.  
That gave us the opportunity to explain to each other our ideas behind the personally created code. Also we could see different approaches for solving same problems.  
Branches are as follow:  
https://gitlab.com/pavlova.dimitrina/weather-forecast-app/tree/weather-forecast-app-didi,  
https://gitlab.com/pavlova.dimitrina/weather-forecast-app/tree/weather-forecast-app-gg,  
https://gitlab.com/pavlova.dimitrina/weather-forecast-app/tree/weather-forecast-app-silvia  

Finally, our team has chosen to present one branch which we think has more features and is written more professionally.

##### Contributors: #####
Didi, Joro and Silvia from Team 12.